---
Title: Welcome
Description: Pico is a stupidly simple, blazing fast, flat file CMS.
Template: index
---

<header id="header">
@[/inc/slider.md]
</header>

<section id="about-us" class="about-us">
@[/inc/aboutus.md]
</section>

<section id="case-study" class="case-study">
@[/inc/casestudy.md]
</section>

<section id="why-choose-us" class="why-choose-us">
@[/inc/whychooseus.md]
</section>

<section id="processes" class="processes">
@[/inc/process.md]
</section>

<section id="call-to-action" class="call-to-action main-bg">
@[/inc/calltoaction.md]
</section>


<section id="testimonial" class="testimonial padding-large white-color text-center">
@[/inc/testimonial.md]
</section>


<section id="portfolio" class="portfolio padding-large text-center">
@[/inc/portfolio.md]
</section>

<section id="promote" class="promote main-bg white-color">
@[/inc/promote.md]
</section>

<section id="promote" class="twitter">
@[/inc/contact.md]
</section>

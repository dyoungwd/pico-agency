
        <div class="container">
            <div class="row">
                <h2 class="margin-bottom-medium">What Our <strong class="bold-text">Customer</strong> Said</h2>
                <div class="col-md-10 col-md-offset-1">

                    <!-- *****  Carousel start ***** -->
                    <div id="testimonial-carousel" class="owl-carousel owl-theme testimonial-carousel">

                        <!-- =========================
                           Single Testimonial item
                        ============================== -->
                        <div class="item margin-bottom-small"> <!-- ITEM START -->
                            <p>Excellent Quality of Work. Good Communication. Very Dedicated...</p>
                            <div class="client margin-top-medium clearfix">
                                <!-- <img src="/img/testimonial/testimonial-1.jpg" height="50" width="50" alt="Client Image"> -->
                                <ul class="client-info main-color">
                                    <li><strong>Amit Talwar</strong></li>
                                    <li>Owner, Amvera Global</li>
                                </ul>
                            </div>
                        </div> <!-- ITEM END -->

                        <!-- =========================
                           Single Testimonial item
                        ============================== -->
                        <div class="item"> <!-- ITEM START -->
                            <p>Darren not only did an excellent job well within the time frame but spent time understanding what I wanted before he accepted the job. Thoroughly recommend Darren for your go to web design guy...</p>
                            <div class="client margin-top-medium">
                               <!-- <img src="/img/testimonial/testimonial-1.jpg" height="50" width="50" alt="Client Image"> -->
                                <ul class="client-info main-color">
                                    <li>Richard Tunnah</li>
                                    <li>Author, Business Consultant</li>
                                </ul>
                            </div>
                        </div> <!-- ITEM END -->

                        <div class="item"> <!-- ITEM START -->
                            <p>A prof guy. always connected and responded to all our needs...</p>
                            <div class="client margin-top-medium">
                                <!-- <img src="/img/testimonial/testimonial-1.jpg" height="50" width="50" alt="Client Image"> -->
                                <ul class="client-info main-color">
                                    <li>Jerry Perez</li>
                                    <li>Easy Hosting</li>
                                </ul>
                            </div>
                        </div> <!-- ITEM END -->

                        
                    </div>
                </div>
            </div>
        </div>
    

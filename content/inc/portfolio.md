

        <div class="container margin-bottom-medium">
            <div class="row margin-bottom-medium wow fadeInUp">
                <h2>
                    Our
                    <strong class="main-color bold-text">Portfolios</strong>
                </h2>
                <div class="line main-bg"></div>

                <div class="col-md-10 col-md-offset-1">
                    <div class="subtitle">Check out some of our latest projects.</div>
                    <p></p>
                </div>
            </div> <!-- *** end row *** -->

            <!-- *****  Portfolio Filters ***** -->
            <div class="filters">
                <ul class="wow lightSpeedIn">
                    <li><a href="#" data-filter="*" class="active"><i class="icon-grid"></i></a></li>
                    <li><a href="#" data-filter=".joomla">Joomla</a></li>
                    <li><a href="#" data-filter=".bootstrap">Bootstrap</a></li>
                    <li><a href="#" data-filter=".responsive">Responsive</a></li>
                    <li><a href="#" data-filter=".design-only">Design Only</a></li>
                    <li><a href="#" data-filter=".ecommerce">eCommerce</a></li>
                </ul>
            </div> <!-- *** end filters *** -->
        </div> <!-- *** end container *** -->

        <!-- *****  Portfolio  wrapper ***** -->
        <div class="portfolio-wrapper margin-bottom-medium">

            <!-- =========================
               Portfolio item
            ============================== -->
            <div class="portfolio-item joomla bootstrap responsive">
                <div class="portfolio">
                    <a href="https://www.behance.net/gallery/35758165/Vixen-Tanning-Joomla" data-lightbox-gallery="portfolio">
                        <img src="img/portfolio/img-vix.png" alt="Vixen Tanning"/><!-- END PORTFOLIO IMAGE -->
                        <div class="portfolio-overlay hvr-rectangle-out">
                            <h2 class="margin-bottom-small">
                                Vixen
                                <strong class="white-color bold-text">Tanning</strong>
                            </h2>
                            <div class="button">View Project</div>
                        </div><!-- END PORTFOLIO OVERLAY -->
                    </a>
               </div>
            </div> <!-- *** end portfolio-item *** -->

            <!-- =========================
               Portfolio item
            ============================== -->
            <div class="portfolio-item responsive">
               <div class="portfolio">
                  <a href="https://www.behance.net/gallery/33192039/JARD-framework" data-lightbox-gallery="portfolio">
                    <img src="img/portfolio/img-jard.png" alt="Portfolio 2"/><!-- END PORTFOLIO IMAGE -->
                    <div class="portfolio-overlay hvr-rectangle-out">
                        <h2 class="margin-bottom-small">
                            JARD
                            <strong class="white-color bold-text">Framework</strong>
                        </h2>
                        <div class="button">View Project</div>
                    </div><!-- END PORTFOLIO OVERLAY -->
                  </a>
               </div>
            </div> <!-- *** end  portfolio-item *** -->

            <!-- =========================
               Portfolio item
            ============================== -->
            <div class="portfolio-item bootstrap responsive ecomerce">
               <div class="portfolio">
                  <a href="https://www.behance.net/gallery/33885496/Responsive-Store-front-Website"   data-lightbox-gallery="portfolio">
                    <img src="img/portfolio/img-prem.png" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                    <div class="portfolio-overlay hvr-rectangle-out">
                        <h2 class="margin-bottom-small">
                            Prem
                            <strong class="white-color bold-text">Fish Oil</strong>
                        </h2>
                        <div class="button">View Project</div>
                    </div><!-- END PORTFOLIO OVERLAY -->
                  </a>
               </div>
            </div> <!-- *** end  portfolio-item *** -->

            <!-- =========================
               Portfolio item
            ============================== -->
            <div class="portfolio-item print">
               <div class="portfolio">
                  <a href="https://www.behance.net/gallery/33478733/Indian-import-business-website" data-lightbox-gallery="portfolio">
                    <img src="img/portfolio/img-ren.png" alt="Portfolio 1"/><!-- END PORTFOLIO IMAGE -->
                    <div class="portfolio-overlay hvr-rectangle-out">
                        <h2 class="margin-bottom-small">

                            <strong class="white-color bold-text">Renlife</strong>
                        </h2>
                        <div class="button">View Project</div>
                    </div><!-- END PORTFOLIO OVERLAY -->
                  </a>
               </div>
            </div> <!-- *** end  portfolio-item *** -->


        </div> <!-- *** end  portfolio-wrapper *** -->

  <center>
<a href="https://www.behance.net/jardwebdesign" ><div class="button  hvr-grow" style="background:#3498DB" color:#fafafa;">View More</div></a></center>

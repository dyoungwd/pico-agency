  <div class="container margin-top-large">
            <h2>
                Why
                <strong class="bold-text">Choose</strong>
                <span class="light-text main-color">Us</span>
            </h2>
            <div class="line main-bg margin-bottom-large"></div>

            <div class="row text-center">

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow slideInLeft">
                        <div class="icon"><i class="fa fa-pencil"></i></div>
                        <h4>Minimal <strong>Design</strong></h4>
                        <p></p>
                    </div>
                </div>

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow fadeInUp">
                        <div class="icon"><i class="fa fa-heart"></i></div>
                        <h4>Love Our <strong>Clients</strong></h4>
                        <p></p>
                    </div>
                </div>

                <!-- ***** Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow slideInRight">
                        <div class="icon"><i class="fa fa-html5"></i></div>
                        <h4><strong>Professional</strong> Design</h4>
                        <p></p>
                    </div>
                </div>

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow slideInLeft">
                        <div class="icon"><i class="fa fa-joomla"></i></div>
                        <h4><strong>Joomla</strong> CMS</h4>
                        <p></p>
                    </div>
                </div>

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow fadeInUp">
                        <div class="icon"><i class="fa fa-code"></i></div>
                        <h4><strong>Web</strong> Development</h4>
                        <p></p>
                    </div>
                </div>

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow slideInRight">
                        <div class="icon"><i class="fa fa-coffee"></i></div>
                        <h4><strong>Coffee</strong> Powered</h4>
                        <p></p>
                    </div>
                </div>

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow slideInLeft">
                        <div class="icon"><i class="fa fa-pencil"></i></div>
                        <h4>FlatFile <strong>CMS</strong></h4>
                        <p></p>
                    </div>
                </div>

                <!-- *****  Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow fadeInUp">
                        <div class="icon"><i class="fa fa-globe"></i></div>
                        <h4>Clients <strong>Worldwide</strong></h4>
                        <p></p>
                    </div>
                </div>

                <!-- ***** Service Single ***** -->
                <div class="col-md-4">
                    <div class="service wow slideInRight">
                        <div class="icon"><i class="fa fa-dollar"></i></div>
                        <h4><strong>e</strong>Commerce</h4>
                        <p></p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div> <!-- *** end row *** -->
        </div> <!-- *** end container *** -->

        <div class="overlay">
            <div class="container padding-large">
                <div class="row">
                    <div class="col-md-5 text-center process-interactive wow fadeInLeft" data-wow-duration="2s">
                        <div class="process-bar main-bg discussion style="color:#fdfdfd;">
                            <i class="fa fa-comments fa-2x"></i>
                        </div>
                        <div class="process-bar right check" style="color:#fdfdfd;">
                            <i class="fa fa-check fa-2x"></i>
                        </div>
                        <div class="lines"></div>
                        <div class="process-bar main-bg idea style="color:#fdfdfd;">
                            <i class="fa fa-lightbulb-o fa-2x"></i>
                        </div>
                        <div class="process-bar right office" style="color:#fdfdfd;">
                            <i class="fa fa-code fa-2x"></i>
                        </div>
                    </div> <!-- *** end col-md-5 *** -->
                    <div class="col-md-7">

                        <!-- *****  Single feature ***** -->
                        <div class="feature wow fadeInUp" data-wow-delay=".2s">
                            <div class="icon-container pull-left">
                                <span class="fa fa-comments"></span>
                            </div>
                            <div class="description text-left pull-right">
                                <h4><strong>Discussion</strong></h4>
                                <p>
                                    The process starts with discussing and finding the requirments of your project - what you require, what you don't require and the best possible solutions to accomplish this. Your input is valued as JARD is <strong>working  with you</strong>.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- *****  Single feature ***** -->
                        <div class="feature wow fadeInUp" data-wow-delay=".3s">
                            <div class="icon-container pull-left">
                                <span class="fa fa-lightbulb-o "></span>
                            </div>
                            <div class="description text-left pull-right">
                                <h4><strong>Get Idea</strong></h4>
                                <p>
                                    The next step in the process involves coming up with a concept for your project. This can include mockups, suggestions, options.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- *****  Single feature ***** -->
                        <div class="feature wow fadeInUp" data-wow-delay=".3s">
                            <div class="icon-container pull-left">
                                <span class="fa fa-check "></span>
                            </div>
                            <div class="description text-left pull-right">
                                <h4><strong>Client Approval</strong></h4>
                                <p>
                                    Once the initia concept is done, it is then passed to you, the client, for approval and any changes you would like to be carried out. We then move on to the the implementation of the concept in to the web site.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <!-- *****  Single feature ***** -->
                        <div class="feature wow fadeInUp" data-wow-delay=".4s">
                            <div class="icon-container pull-left">
                                <span class="fa fa-code "></span>
                            </div>
                            <div class="description text-left pull-right">
                                <h4><strong>Implementation</strong></h4>
                                <p>
                                  This is the final stage of the process, where the website is built and changes from the previous stage done. Revisions can of course still be made at this stage if required. Once the site has its final approval, it shall be uploaded to your server and tested to ensure all functions are working correctly.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div> <!-- *** end col-md-7 *** -->
                </div>
            </div>
        </div>
    </section> <!-- *** end Processes *** -->

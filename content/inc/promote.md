
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-1 col-md-5 col-sm-4 text-center">
                    <p class="light-text">Like what you see so far?</p>
                </div>
                <div class="col-lg-6 col-lg-offset-1 col-md-7 col-sm-8 button-container">
                    <a href="#" class="button deep hvr-grow">Request a Quote</a>
                    <span class="margin-right-small margin-left-small">or</span>
                    <a href="#" class="button light hvr-grow">Hire Us</a>
                </div>
            </div>
        </div>


<div class="container padding-large">
 <div class="container margin-bottom-medium">
      <div class="row margin-bottom-medium wow fadeInUp ">
          <h2 class="text-center">
              Contact
              <strong class="main-color bold-text text-center">Us</strong>
          </h2>
          
          </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 wow fadeInLeft">

                        <div class="clearfix"></div>
                        <!-- *****  Contact form ***** -->
                            <form class="form">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" id="first-name" placeholder="First name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" id="last-name" placeholder="Last name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="email" class="form-control" id="email" placeholder="Email address">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" id="project-name" placeholder="Project name">
                                    </div>
                                    <div class="form-group col-md-12 mab-none">
                                        <textarea rows="6" class="form-control" id="description" placeholder="Your project details and description ..."></textarea>
                                    </div>
                                    <div class="form-group offset-col-md-8" style="padding-top:15px; float:right;">
                                        <div class="button bold-text main-bg"><i class="fa fa-envelope"></i></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

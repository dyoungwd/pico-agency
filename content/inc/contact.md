        <div class="overlay">
            <div class="container padding-large text-center">
            <div class="container margin-bottom-medium">
                 <div class="row margin-bottom-medium wow fadeInUp ">
                     <h2 class="text-center">
                         Contact
                         <strong class="main-color bold-text text-center">Us</strong>
                     </h2>

                     </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 wow fadeInLeft">

                        <div class="clearfix"></div>
                        <!-- *****  Contact form ***** -->
                        <form id="contact-form" method="post" action="http://jardwebdesign.co.uk/contact.php" role="form">

                      <div class="messages"></div>

                      <div class="controls">

                          <div class="row">
                              <div class="col-md-6">
                                  <label for="form_name"></label>
                                  <input id="form_name" type="text" name="name" class="form-control" placeholder="First Name" required="required">
                              </div>
                              <div class="col-md-6">
                                  <label for="form_lastname"></label>
                                  <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Last Name " required="required">
                              </div>
                              <div class="col-md-6">
                                  <label for="form_email"></label>
                                  <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required">
                              </div>
                              <div class="col-md-6">
                                  <label for="form_phone"></label>
                                  <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Project Name">
                              </div>
                              <div class="col-md-12">
                                  <label for="form_message"></label>
                                  <textarea id="form_message" name="message" class="form-control" placeholder="Your Message*" rows="4" required="required"></textarea>
                              </div>
                              <div class="col-md-12" style="padding:10px;"></div>
                              <div class="col-md-12">
                                  <input type="submit" class="btn ghost-button " value="Get In Touch">
                              </div>

                          </div>
                      </div>

                  </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

   <div class="overlay">
            <div class="container padding-top-large">
                <h2>
                    <strong class="bold-text">About</strong>
                    <span class="light-text main-color">US</span>
                </h2>
                <div class="line main-bg"></div>
                <div class="row margin-bottom-medium">
                    <div class="col-md-7">
                        <div class="jumbo-text light-text margin-top-medium wow slideInLeft" data-wow-duration="2s">
                            JARD, A Digital <strong class="bold-text">Web Development Agency</strong>
                            To Help You Reach Your Potential
                        </div>
                    </div>
                    <div class="col-md-5">
                        <img src="img/Laptop_09.png" alt="About Us Big Image" class="center-block img-responsive">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <!-- <p class="margin-bottom-medium wow slideInUp"><</p> -->
                <div class="row margin-top-large">
                <div class="col-md-4">
                    <img src="img/case-studdy-people.png" class="center-block img-responsive" alt="Case Study">
                </div>
                    <div class="col-md-8">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                            <!-- =========================
                               Collapsible Panel 1
                            ============================== -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <div class="panel-title">
                                        <a href="#collapseOne" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">
                                            <span class="state"><strong>-</strong></span>
                                            <strong>Who Is JARD</strong>
                                        </a>
                                    </div>
                                </div> <!-- *** end panel-heading *** -->
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        I am a freelance front-end web developer based in Tayside, Scotland. My aim is to make the web easier for you and clearer for you and your customers.
                                    </div>
                                </div> <!-- *** end collapsed item *** -->
                            </div> <!-- *** end panel *** -->

                            <!-- =========================
                              Collapsible Panel 2
                            ============================== -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <div class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span class="state"><strong>+</strong></span>
                                            <strong>What does JARD do</strong>
                                        </a>
                                    </div>
                                </div> <!-- *** end panel-heading *** -->
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        JARD Delivers professional web development solutions for individuals, small and medium businesses and  charitable / community groups. <br /> <br />

                                        When developing sites, JARD can provide static or content managed (by way of <a href="http://joomla.org">Joomla</a>). I ensure all web sites developed are all keeping "mobile first" and fully responsive in mind and can use a multitude of different responsive frameworks including Bootstrap, MatrerelizeCss, Material Lite and my own light-weight grid framework.<br /> <br />

                                        JARD can also offer a full eCommerce web site for your online business using a variety of systems with integrated online payment solutions.<br />
                                    </div>
                                </div> <!-- *** end collapsed item *** -->
                            </div> <!-- *** end panel *** -->

                            <!-- =========================
                              Collapsible Panel 3
                            ============================== -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <div class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <span class="state"><strong>+</strong></span>
                                            <strong>Where Can JARD Work</strong>
                                        </a>
                                    </div>
                                </div> <!-- *** end panel-heading *** -->
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                      There is no limit to where you are based. JARD has worked with clients from all over the globe, from the UK, USA, Singapore, Isreal and China to name a few. <br /> <br />

                                      Communication on your project is done in a timely fashion using a variety of methods to suit yourself such as Skype, Slack, Trello and email.
                                    </div>
                                </div> <!-- *** end collapsed item *** -->
                            </div> <!-- *** end panel *** -->
                        </div> <!-- *** end panel-group *** -->
                    </div> <!-- *** end col-md-8 *** -->

                </div>
            </div>
        </div>

 <div class="row mar-none mat-none">

            <!-- *****  Case Study Left ***** -->
            <div class="col-md-6 case-study-left wow slideInLeft">
                <div class="overlay padding-large text-right">
                    <div class="description">
                        <h3 class="margin-bottom-small light-text" style="color:#484848;">Exciting websites, designed with passion</h3>
                        <p></p>
                    </div>
                </div>
            </div>

            <!-- *****  Case Study Right ***** -->
            <div class="col-md-6 case-study-right wow slideInRight">
                <div class="overlay padding-large">
                    <div class="description">
                        <h3 class="margin-bottom-small light-text" style="color:#fdfdfd;">Its time to turn your ideas into reality</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

Pico-Agency Theme
====

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/picocms/Pico/blob/master/LICENSE)
[![Version](https://img.shields.io/badge/version-1.0-lightgrey.svg)](https://github.com/picocms/Pico/releases/latest)
[![Build Status](https://travis-ci.org/picocms/Pico.svg)](https://travis-ci.org/picocms/Pico)

This is a modular single page theme for Pico - a stupidly simple, blazing fast, flat file CMS. See http://picocms.org/ for more info.


Install
-------
#### Using this pre-bundled theme
This theme comes equipped with the latest realease of Pico CMS along with the default theme, Pico-Agency and sample content. Simply download the zip and upload to your server.

#### You don't have a web server?
Starting with PHP 5.4 the easiest way to try Pico is using [the built-in web server of PHP][PHPServer]. Please note that PHPs built-in web server is for development and testing purposes only!

###### Step 1
Navigate to Pico's installation directory using a shell.

###### Step 2
Start PHPs built-in web server:
```shell
$ php -S 127.0.0.1:8080
```

###### Step 3
Access Pico from http://localhost:8080.


------------

#### Plugins Used
This theme comes pre-bundled with the pico nested pages plugin by coofercat: https://github.com/coofercat/pico-nestedpages to enable the modular content.

It also includes Font Awesome, Owl carousel and Nivo lightbox.

#### Editing the Theme
The theme comes with different colour schemes that can be change by using a different main css file. Just change the css file required via the themes/pico-agency/index.html file.

The content can be edited via the .md files in the content/inc folder. To disable or re-arrange the blocks, just remove or change position of the blocks via the content/index.md file.

#### You still need help or experience a problem with this theme?
If you are having issues, please email me at dev@biscloud.space

#### Demo

New Demo Coming Shortly
